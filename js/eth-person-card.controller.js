export class ethPersonCardController {
    // 99117239392005503 - multiple persons External Data
    // 990066667580205503 or 991130796959705501 - Alma
    // 991102492829705501 - multiple persons Alma
    constructor( ethConfigService, ethPersonCardConfig, ethPersonCardService, $scope, $compile, $anchorScroll, $location) {
        this.ethConfigService = ethConfigService;
        this.config = ethPersonCardConfig;
        this.ethPersonCardService = ethPersonCardService;
        this.$scope = $scope;
        this.$compile = $compile;
        this.$anchorScroll = $anchorScroll;
        this.$location = $location;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.lang = this.ethConfigService.getLanguage();
            this.doCheck = true;
            // array of gnd ids in the details section
            this.gndIds = [];
            this.idRefs = [];
            // array of person objects
            this.persons = [];
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardController.$onInit()\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            // wait until details informations are loaded
            if(!this.parentCtrl._details || !this.doCheck)
            {
                return;
            }
            // parentCtrl._details is loaded -> work should be done only once
            this.doCheck = false;

            if((!this.parentCtrl.item.pnx.display.lds03 || this.parentCtrl.item.pnx.display.lds03.length === 0) && (!this.parentCtrl.item.pnx.display.lds90 || this.parentCtrl.item.pnx.display.lds90.length === 0))return;

            // IdRef - lds90
            if((this.parentCtrl.item.pnx.display.lds90 && this.parentCtrl.item.pnx.display.lds90.length > 0)){
                let lds90 = this.parentCtrl.item.pnx.display.lds90;
                for(let i = 0; i < lds90.length; i++){
                    let part = lds90[i].substring(lds90[i].indexOf('idref.fr/') + 9);
                    part = part.substring(0, part.indexOf('">'));
                    if(this.idRefs.indexOf(part)===-1)this.idRefs.push(part);
                }
            }
            for(var i = 0; i < this.idRefs.length; i++){
                let idref = this.idRefs[i];
                if (idref === "")continue;
                // get gnd by idref (via viaf)
                this.ethPersonCardService.getGndByIdRef(idref)
                    .then((data) => {
                        if(data && data.indexOf('d-nb.info/')>-1){
                            let gnd = data.substring(data.lastIndexOf('/')+1)
                            this.ethPersonCardService.getPersonByGnd(gnd, this.lang)
                                .then((data) => {
                                    data.idref = idref;
                                    this.renderPersonLink(data, "sudoc");
                                })                
                        }
                    })                
            }

            // GND - lds03
            let aLds03Details = this.parentCtrl._details.filter( d => {
                if(d.label === 'lds03'){return true;}
                else {return false;}
            })
            aLds03Details[0].values[0].values.forEach( l => {
                // gnd link
                if(l.indexOf('href="http://d-nb.info/gnd/')>-1){
                    let href1 = l.substring(l.indexOf('href="') + 6);
                    let href = href1.substring(0,href1.indexOf('"'));
                    let gnd = href.substring(href.lastIndexOf('/') + 1)
                    this.gndIds.push(gnd);
                }
                // gnd text (name: gndid)
                else if (l.indexOf(': ') > -1) {
                    let part = l.substring(l.indexOf(': ') + 2);
                    if(part.indexOf('(DE-588)') > -1){
                        this.gndIds.push(part.replace('(DE-588)',''));
                    }
                    else{
                        this.gndIds.push(part);
                    }
                }
            })
            // request for informations from various providers
            this.gndIds.forEach(gnd => {
                this.ethPersonCardService.getPersonByGnd(gnd, this.lang)
                    .then((data) => {
                        this.renderPersonLink(data, "gnd");   
                    });
            })
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardController.$doCheck()\n\n");
            console.error(e.message);
        }
    }

    renderPersonLink(data, type){
        try{
            if(!data || !data.results || data.results.length === 0 || (!data.gnd && !data.idref)){
                return;
            }
            let linktext = this.ethConfigService.getLabel(this.config, 'personCardLink');
            // construct a person object from different data sources
            let p = this.ethPersonCardService.processPersonsResponse(data.results, this.ethConfigService, this.config, this.lang);
            if(type === 'gnd'){
                p.gnd = data.gnd;
                p.id = data.gnd;
            }
            else if (type === 'sudoc'){
                p.idref = data.idref; 
                p.id = data.idref;
            }

            // is p realy a person (or a subject ...)?
            if(type === "gnd" && p.gnd && ((p.entityfacts && p.entityfacts.preferredName) || (p.wiki &&p.wiki.label) )  && !document.getElementById('personcard' + p.gnd)){
                this.persons.push(p);
                // insert a person card toggle link next to the GND link
                // ALMA: GND Link
                let aGNDLinks = document.querySelectorAll(".item-details-element a[href='http://d-nb.info/gnd/" + p.gnd + "']");
                if(aGNDLinks.length > 0){
                    aGNDLinks.forEach(a => {
                        a.parentNode.classList.add("eth-personcard-link-row");
                        p.detailsGndLink = a;
                    });
                    if(p.detailsGndLink){
                        let html = `
                        <md-button id="personcardlink${p.gnd}" class="eth-personcard-link md-primary" ng-click="$ctrl.toggleCard('${p.gnd}')" aria-controls="personcard${p.gnd}" aria-expanded="false">
                            <span>${linktext}</span>
                            <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                            <div class="personcard-link__down"/>
                        </md-button>
                        `;
                        angular.element(p.detailsGndLink).after(this.$compile(html)(this.$scope));
                    }
                }
                else{
                    // External Data: Gnd part of text
                    let aDetailsParts = Array.from(document.querySelectorAll('prm-service-details div[ng-repeat]'));
                    let aGNDParts = aDetailsParts.filter(e => {
                        let label = e.querySelector("[data-details-label]");
                        if(label && label.getAttribute("data-details-label") === "lds03"){
                            return true;
                        }
                        else{
                            return false;
                        }
                    })
                    if(aGNDParts.length === 0){
                        return;
                    }
                    let aSpan = document.querySelectorAll("prm-service-details div[ng-repeat] .item-details-element span");
                    aSpan.forEach(s => {
                        if(s && s.textContent && s.textContent.indexOf(p.gnd) > -1){
                            if(!document.getElementById('personpage' + p.gnd)){
                                s.parentNode.classList.add("eth-personcard-link-row");
                                p.detailsGndLink = s;
                                let html = `
                                <md-button id="personcardlink${p.gnd}" class="eth-personcard-link md-primary" ng-click="$ctrl.toggleCard('${p.gnd}')" aria-controls="personcard${p.gnd}" aria-expanded="false">
                                    <span>${linktext}</span>
                                    <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                                    <div class="personcard-link__down"/>
                                </md-button>
                                `;
                                angular.element(p.detailsGndLink).after(this.$compile(html)(this.$scope));
                            }
                        }
                    })
                }
            }
            else if(type === "sudoc" && p.idref && ((p.wiki &&p.wiki.label))  && !document.getElementById('personcard' + p.idref)){
                this.persons.push(p);
                // insert a person card toggle link next to the IdRef link
                let aIdRefLinks = document.querySelectorAll(".item-details-element a[href='https://www.idref.fr/" + p.idref + "']");
                if(aIdRefLinks.length > 0){
                    aIdRefLinks.forEach(a => {
                        a.parentNode.classList.add("eth-personcard-link-row");
                        p.detailsIdRefLink = a;
                    });
                    if(p.detailsIdRefLink){
                        let html = `
                        <md-button id="personcardlink${p.idref}" class="eth-personcard-link md-primary" ng-click="$ctrl.toggleCard('${p.idref}')" aria-controls="personcard${p.idref}" aria-expanded="false">
                            <span>${linktext}</span>
                            <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                            <div class="personcard-link__down"/>
                        </md-button>
                        `;
                        angular.element(p.detailsIdRefLink).after(this.$compile(html)(this.$scope));
                    }
                }
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardController renderPersonLink: \n\n");
            console.error(e.message);
            throw(e);
        }
    }   
     
    toggleCard(gnd){
        let cont = document.getElementById('personcard' + gnd);
        if(!cont)return;
        let link = document.getElementById('personcardlink' + gnd);
        angular.element(cont).toggleClass('eth-person__facts-visible');
        angular.element(link).toggleClass('eth-personcard-link-opened');
        if(angular.element(link).hasClass('eth-personcard-link-opened')){
            this.scrollTo('personcard' + gnd + '-anchor');
            angular.element(link).attr('aria-expanded','true');
        }
        else{
            angular.element(link).attr('aria-expanded','false');
        }
        return;
    }

    scrollTo(anchorName){
        this.$anchorScroll.yOffset = 200;
        this.$anchorScroll(anchorName);
        return;
    }
}

ethPersonCardController.$inject = ['ethConfigService', 'ethPersonCardConfig', 'ethPersonCardService', '$scope', '$compile', '$anchorScroll', '$location'];
