export const ethPersonCardConfig = function(){
    return {
        whitelistMetagrid: ["hallernet", "fotostiftung", "sikart","elites-suisses-au-xxe-siecle","bsg", "dodis", "helveticarchives", "helveticat", "hls-dhs-dss", "histoirerurale","lonsea","ssrq","alfred-escher","geschichtedersozialensicherheit"],
        sourcesMetagrid: {
            label: {
                'hallernet': {
                    de: 'Editions- und Forschungsplattform hallerNet',
                    en: 'Editions- und Forschungsplattform hallerNet',
                    fr: 'Editions- und Forschungsplattform hallerNet',
                    it: 'Editions- und Forschungsplattform hallerNet'
                },
                'fotostiftung': {
                    de: 'Fotostiftung Schweiz',
                    en: 'Fotostiftung Schweiz',
                    fr: 'Fotostiftung Schweiz',
                    it: 'Fotostiftung Schweiz'
                },
                'sikart': {
                    de: 'SIKART',
                    en: 'SIKART',
                    fr: 'SIKART',
                    it: 'SIKART'
                },
                'elites-suisses-au-xxe-siecle': {
                    de: 'Schweizerische Eliten im 20. Jahrhundert',
                    en: 'Swiss elites database',
                    fr: 'Elites suisses au XXe siècle',
                    it: 'Elites suisses au XXe siècle'
                },
                'bsg': {
                    de: 'Bibliographie der Schweizergeschichte',
                    en: 'Bibliography on Swiss History',
                    fr: 'Bibliographie de l\'histoire suisse',
                    it: 'Bibliografia della storia svizzera'

                },
                'dodis': {
                    de: 'Diplomatische Dokumente der Schweiz',
                    en: 'Diplomatic Documents of Switzerland',
                    fr: 'Documents diplomatiques suisses',
                    it: 'Documenti diplomatici svizzeri'
                },
                'helveticat': {
                    de: 'Helveticat',
                    en: 'Helveticat',
                    fr: 'Helveticat',
                    it: 'Helveticat'
                },
                'hls-dhs-dss': {
                    de: 'Historisches Lexikon der Schweiz',
                    en: 'Historical Dictionary of Switzerland',
                    fr: 'Dictionnaire historique de la Suisse',
                    it: 'Dizionario storico della Svizzera'
                },
                'histoirerurale': {
                    de: 'Archiv für Agrargeschichte',
                    en: 'Archives of rural history',
                    fr: 'Archives de l\'histoire rurale',
                    it: 'Archivio della storia rurale'
                },
                'lonsea': {
                    de: 'Lonsea',
                    en: 'Lonsea',
                    fr: 'Lonsea',
                    it: 'Lonsea'
                },
                'ssrq': {
                    de: 'Sammlung Schweizerischer Rechtsquellen',
                    en: 'Collection of Swiss Law Sources',
                    fr: 'Collection des sources du droit suisse',
                    it: 'Collana Fonti del diritto svizzero'
                },
                'alfred-escher': {
                    de: 'Alfred Escher-Briefedition',
                    en: 'Alfred Escher letters edition',
                    fr: 'Edition des lettres Alfred Escher',
                    it: 'Edizione lettere Alfred Escher'
                },
                'geschichtedersozialensicherheit': {
                    de: 'Geschichte der sozialen Sicherheit',
                    en: 'Geschichte der sozialen Sicherheit',
                    fr: 'Histoire de la sécurité sociale',
                    it: 'Storia della sicurezza sociale'
                }
            }
        },
        label: {
            personCardLink: {
                de: 'Mehr Informationen zur Person',
                en: 'More informations'
            },
            closePersonCard: {
                de: 'Personenkarte schliessen',
                en: 'Close personcard'
            },
            imgLicense: {
                de: 'Lizenz für das Bild siehe',
                en: 'Information regarding the license status of embedded media files',
            },
            moreInformations: {
                de: 'Mehr Informationen zur Person',
                en: 'More information about the person'
            },
            infoGndWd: {
                de: 'Informationen aus Wikidata und der GND',
                en: 'Information from Wikidata and the GND'
            },
            archiveLinks: {
                de: 'Links in Archive',
                en: 'Links in archives'
            },
            researcherProfile: {
                de: 'Forscherprofile via IDs aus Wikidata',
                en: 'Researcher profiles via IDs from Wikidata'
            },
            linkOrcid: {
                de: 'ORCID Profil',
                en: 'ORCID record'
            },
            linkPublon: {
                de: 'Publons Profil',
                en: 'Publons profile'
            },
            linkScholar: {
                de: 'Google Scholar Profil',
                en: 'Google Scholar profile'
            },
            linkScopus: {
                de: 'Scopus author details',
                en: 'Scopus author details'
            },
            linkResearchgate: {
                de: 'ResearchGate Profil',
                en: 'ResearchGate profile'
            },
            linkMendeley: {
                de: 'Mendeley Profil',
                en: 'Mendeley profile'
            },
            linkDimension: {
                de: 'Dimensions Profil',
                en: 'Dimensions profile'
            },
            linksWD: {
                de: 'Links aus Wikidata',
                en: 'Links from Wikidata'
            },
            linksMetagrid: {
                de: 'Links von Metagrid',
                en: 'Links from Metagrid'
            },
            born: {
                de: 'Geboren',
                en: 'Born'
            },
            died: {
                de: 'Gestorben',
                en: 'Died'
            }
        }
    }
}
