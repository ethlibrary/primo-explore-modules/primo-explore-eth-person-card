import 'primo-explore-eth-person-card';

var app = angular.module('viewCustom', ['ethPersonCardModule'])
    .component('prmServiceDetailsAfter',  {
        bindings: {parentCtrl: '<'},
        template: `<eth-person-card-component after-ctrl="$ctrl"></eth-person-card-component>`
    })
