/**
* @ngdoc module
* @name ethPersonCardModule
*
* @description
*
* - enhances fullview by person cards
* - based on gnd id in lds03
* - checks metagrid, entityfacts (DNB), wikidata for person informations and links
*
* <b>AngularJS Dependencies</b><br>
* Service ./services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPersonCardConfig}<br>
* Service {@link ETH.ethPersonCardService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS css/eth-person-card.css
* IMG img/chevron_down.png
* IMG img/close1.png
* IMG img/close2.png
*
*
*/
import {ethConfigService} from './services/eth-config.service';
import {ethPersonCardConfig} from './eth-person-card.config';
import {ethPersonCardHtml} from './eth-person-card.html';
import {ethPersonCardService} from './eth-person-card.service';
import {ethPersonCardController} from './eth-person-card.controller';

export const ethPersonCardModule = angular
    .module('ethPersonCardModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethPersonCardConfig', ethPersonCardConfig)
        .factory('ethPersonCardService', ethPersonCardService)
        .controller('ethPersonCardController', ethPersonCardController)
        .component('ethPersonCardComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethPersonCardController',
            template: ethPersonCardHtml
        })

