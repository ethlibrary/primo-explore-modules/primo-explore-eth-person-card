export const ethPersonCardService = ['$http', '$q', function($http, $q){
    
    function getGndByIdRef(idref){
        let url = 'https://www.viaf.org/viaf/sourceID/SUDOC|' + idref + '/viaf.json';
        return $http.get(url)
            .then(
                function(response){
                    let aSudoc = response.data.sources.source.filter(s => {
                        return s['#text'].indexOf('DNB|') > -1;
                    })
                    if(aSudoc.length > -1){
                        return aSudoc[0]['@nsid'];
                    }
                    else{
                        return null;
                    }
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethComposeEraraService.getGndByIdRef: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPersonByGnd(gnd, lang){
        // promise for Metagrid
        let url1 = "https://api.metagrid.ch/search?group=1&skip=0&take=50&provider=gnd&query=" + gnd;
        let promise1 = $http.get(url1)
            .then(function (response) {
                return response;
            });

        // promise for Entityfacts
        let url2 = "https://hub.culturegraph.org/entityfacts/" + gnd;
        let promise2 = $http.get(url2)
            .then(function (response) {
                return response;
            });

        // promise for Wikidata
        let sparqlQuery1 = `SELECT ?item ?itemLabel ?itemDescription ?birth ?death ?birthplaceLabel ?deathplaceLabel ?image ?gnd ?sfa ?hls ?loc ?archived ?wc ?orcid ?publonid ?scholar ?scopus ?researchgate ?mendeley ?dimension ?ordinal
                (group_concat(DISTINCT(?aliasLabel) ;separator = "|") as ?aliasList )
        WHERE {
          ?item wdt:P227 "${gnd}".
          ?item wdt:P31 wd:Q5.
          OPTIONAL {?item wdt:P569 ?birth.}
          OPTIONAL {?item wdt:P19 ?birthplace.}
          OPTIONAL {?item wdt:P570 ?death.}
          OPTIONAL {?item wdt:P20 ?deathplace.}
          OPTIONAL {?item wdt:P18 ?image.}
          OPTIONAL {?item wdt:P227 ?gnd.}
          OPTIONAL {?item wdt:P3889 ?sfa.}
          OPTIONAL {?item wdt:P902 ?hls.}
          OPTIONAL {?item wdt:P244 ?loc.}
          OPTIONAL {?item wdt:P485 ?archived.}
          OPTIONAL {?item wdt:P373 ?wc.}
          OPTIONAL {?item wdt:P496 ?orcid.}
          OPTIONAL {?item wdt:P3829 ?publonid.}
          OPTIONAL {?item wdt:P1960 ?scholar.}
          OPTIONAL {?item wdt:P1153 ?scopus.}
          OPTIONAL {?item wdt:P2038 ?researchgate.}
          OPTIONAL {?item wdt:P3835 ?mendeley.}
          OPTIONAL {?item wdt:P6178 ?dimension.}

          OPTIONAL {?item (rdfs:label|skos:altLabel|wdt:P1449) ?alias.}
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${lang}".
                                  ?alias rdfs:label ?aliasLabel .
                                  ?item schema:description ?itemDescription .
                                  ?item rdfs:label ?itemLabel .}
        }
        GROUP BY ?item ?itemLabel ?itemDescription ?birth ?death ?birthplaceLabel ?deathplaceLabel ?image ?gnd ?sfa ?hls ?loc ?archived ?wc ?orcid ?publonid ?scholar ?scopus ?researchgate ?mendeley ?dimension ?ordinal
         ORDER BY ASC(?ordinal)
         LIMIT 100
        `;
        let url3 = "https://query.wikidata.org/sparql?query=" + sparqlQuery1;
        let promise3 = $http.get(url3)
            .then(function (response) {
                return response;
            });

        // promise for Wikidata 'archived at'
        let sparqlQuery2 = `
        SELECT ?gnd ?statement ?archived ?archivedLabel ?refnode  ?ref ?inventoryno
        where{
            ?item wdt:P227 "${gnd}".
            ?item p:P485 ?statement.
            ?item wdt:P227 ?gnd.
            ?statement ps:P485 ?archived.
            OPTIONAL{?statement pq:P217 ?inventoryno.}
            OPTIONAL{ ?statement prov:wasDerivedFrom ?refnode.
              ?refnode pr:P854 ?ref.
            }
            SERVICE wikibase:label { bd:serviceParam wikibase:language "${lang}". }
        }`;
        let url4 = "https://query.wikidata.org/sparql?query=" + sparqlQuery2;
        let promise4 = $http.get(url4)
            .then(function (response) {
                return response;
            });

        // a rejected promise (e.g. 404) should not reject all promises
        // https://gist.github.com/mzipay/78ce922b717d60899953
        return $q(function(resolve, reject) {
            $q.all(
                [
                promise1.then(keepResponse).catch(keepError),
                promise2.then(keepResponse).catch(keepError),
                promise3.then(keepResponse).catch(keepError),
                promise4.then(keepResponse).catch(keepError)
                ]
            )
            .then(function(responses) {
                resolve(responses);
            })
        })
        .then(function(responses) {
            return buildResult(responses,gnd,"gnd");
        })
        .catch(function(e) {
            console.error("***ETH*** an error occured: ethPersonCardsService.getPersonByGnd(): \n\n");
            console.error(e.message);
            throw(e);
            return null;
        })

    }

    function buildResult(responses,id,type){
        let resp = {};
        if(type === 'gnd'){
            resp.gnd = id;
        }else{
            resp.idref = id;
        }
        resp.qid = [];
        resp.results = [];
        // todo gnd für idref rein
        responses.forEach(r => {
            let o = {'provider':'','gnd':'','resp':{},'status':''};
            // entityfacts
            if(r.data && r.data['@context'] && r.data['@context'].indexOf('hub.culturegraph.org') > -1){
                o.resp = r.data;
                o.status = r.status;
                o.gnd = r.data['@id'].substring(r.data['@id'].lastIndexOf('/') + 1);
                o.provider = 'hub.culturegraph.org';
                resp.results.push(o);
            }
            // metagrid
            else if(r.data && r.data.meta && r.data.meta.uri && r.data.meta.uri.indexOf('api.metagrid.ch') > -1){
                o.resp = r.data;
                o.status = r.status;
                o.gnd = r.data.meta.uri.substring(r.data.meta.uri.indexOf('query=') + 6);
                o.gnd = o.gnd.substring(0, o.gnd.indexOf('&'));
                o.provider = 'api.metagrid.ch';
                resp.results.push(o);
            }
            // wikidata query service
            else if(r.data && r.data.head && r.data.head.vars) {
                o.provider = 'query.wikidata.org';
                o.status = r.status;
                if(r.data.results.bindings.length > 0){
                    o.resp = r.data;
                    if(r.data.results.bindings[0].gnd){
                        o.gnd = r.data.results.bindings[0].gnd.value;
                    }
                    if(r.data.results.bindings[0].birth && r.data.results.bindings[0].item){
                        resp.qid.push(r.data.results.bindings[0].item.value.substring(r.data.results.bindings[0].item.value.lastIndexOf('/')+1));
                    }
                    resp.results.push(o);
                }
            }
        })
        return resp;
    }


    function processEntityfactsResponse(entityfactsResult){
        try{
            let entityfacts = {};
            if (entityfactsResult[0].resp['@type'] !== 'person')return null;
            if(entityfactsResult[0].resp.preferredName)entityfacts.preferredName = entityfactsResult[0].resp.preferredName;
            if(entityfactsResult[0].resp.biographicalOrHistoricalInformation)entityfacts.biographicalOrHistoricalInformation = entityfactsResult[0].resp.biographicalOrHistoricalInformation;
            if(entityfactsResult[0].resp.professionOrOccupation)entityfacts.professionOrOccupation = entityfactsResult[0].resp.professionOrOccupation[0].preferredName;
            if(entityfactsResult[0].resp.placeOfBirth)entityfacts.placeOfBirth = entityfactsResult[0].resp.placeOfBirth[0].preferredName;
            if(entityfactsResult[0].resp.dateOfBirth)entityfacts.dateOfBirth = entityfactsResult[0].resp.dateOfBirth;
            if(entityfactsResult[0].resp.placeOfDeath)entityfacts.placeOfDeath = entityfactsResult[0].resp.placeOfDeath[0].preferredName;
            if(entityfactsResult[0].resp.dateOfDeath)entityfacts.dateOfDeath = entityfactsResult[0].resp.dateOfDeath;
            if(entityfactsResult[0].resp.depiction)entityfacts.depiction = entityfactsResult[0].resp.depiction;
            return entityfacts;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processEntityfactsResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processWikiResponse(wikiResult, ethConfigService, config){
        try{
            if(!wikiResult[0] || !wikiResult[0].resp.results.bindings || wikiResult[0].resp.results.bindings.length === 0){
                return;
            }
            let wiki = {};
            let binding = wikiResult[0].resp.results.bindings[0];
            wiki.qid = binding.item ? binding.item.value.substring(binding.item.value.lastIndexOf('/')+1) : null;
            wiki.label = binding.itemLabel ? binding.itemLabel.value : null;
            wiki.image_url = binding.image ? binding.image.value : null;
            wiki.description = binding.itemDescription ? binding.itemDescription.value : null;
            wiki.gnd = binding.gnd ? binding.gnd.value : null;
            wiki.birth = binding.birth ? binding.birth.value : null;
            wiki.death = binding.death ? binding.death.value : null;
            wiki.birthplace = binding.birthplaceLabel ? binding.birthplaceLabel.value : null;
            wiki.deathplace = binding.deathplaceLabel ? binding.deathplaceLabel.value : null;
            wiki.aVariants = binding.aliasList ? binding.aliasList.value.split('|') : null;
            if(wiki.aVariants && wiki.aVariants.indexOf(wiki.label) === -1){
                wiki.aVariants.unshift(wiki.label);
            }
            wiki.links = [];
            if(binding.item && binding.item.value)wiki.links.push({'url': binding.item.value, 'label': 'Wikidata'});
            if(binding.wc && binding.wc.value)wiki.links.push({'url': 'https://commons.wikimedia.org/wiki/Category:' + binding.wc.value , 'label': 'Wikimedia Commons'});
            if(binding.hls && binding.hls.value)wiki.links.push({'url': 'http://www.hls-dhs-dss.ch/textes/d/D' + binding.hls.value + '.php', 'label': 'Historisches Lexikon der Schweiz'});
            if(binding.gnd && binding.gnd.value)wiki.links.push({'url': 'https://d-nb.info/gnd/' + binding.gnd.value, 'label': 'GND (Gemeinsame Normdatei der Deutschen Nationalbibliothek)'});
            if(binding.sfa && binding.sfa.value)wiki.links.push({'url': 'https://www.swiss-archives.ch/archivplansuche.aspx?ID=' + binding.sfa.value, 'label': 'Schweizerisches Bundesarchiv'});
            if(binding.loc && binding.loc.value)wiki.links.push({'url': 'http://id.loc.gov/authorities/names/' + binding.loc.value + '.html', 'label': 'Library of Congress'});
            wiki.profiles = [];
            if(binding.orcid && binding.orcid.value)wiki.profiles.push({'url': 'https://orcid.org/' + binding.orcid.value, 'label': ethConfigService.getLabel(config, 'linkOrcid')});
            if(binding.publonid && binding.publonid.value)wiki.profiles.push({'url': 'https://publons.com/researcher/' + binding.publonid.value, 'label': ethConfigService.getLabel(config, 'linkPublon')});
            if(binding.scholar && binding.scholar.value)wiki.profiles.push({'url': 'https://scholar.google.com/citations?user=' + binding.scholar.value, 'label': ethConfigService.getLabel(config, 'linkScholar')});
            if(binding.scopus && binding.scopus.value)wiki.profiles.push({'url': 'https://www.scopus.com/authid/detail.uri?authorId=' + binding.scopus.value, 'label': ethConfigService.getLabel(config, 'linkScopus')});
            if(binding.researchgate && binding.researchgate.value)wiki.profiles.push({'url': 'https://www.researchgate.net/profile/' + binding.researchgate.value, 'label': ethConfigService.getLabel(config, 'linkResearchgate')});
            if(binding.mendeley && binding.mendeley.value)wiki.profiles.push({'url': 'https://mendeley.com/profiles/' + binding.mendeley.value, 'label': ethConfigService.getLabel(config, 'linkMendeley')});
            if(binding.dimension && binding.dimension.value)wiki.profiles.push({'url': 'https://app.dimensions.ai/discover/publication?and_facet_researcher=ur.' + binding.dimension.value, 'label': ethConfigService.getLabel(config, 'linkDimension')});
            return wiki;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processWikiResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processwikiArchivesAtResponse(wikiResult){
        try{
            let wikiArchivesAtLinks = [];
            let bindings = wikiResult[0].resp.results.bindings;
            for(let i = 0; i < bindings.length; i++){
                let url = bindings[i].ref ? bindings[i].ref.value: null;
                let label =  bindings[i].archivedLabel ? bindings[i].archivedLabel.value : null;
                let inventoryno =  bindings[i].inventoryno ? bindings[i].inventoryno.value : null;
                wikiArchivesAtLinks.push({'url': url, 'label': label, 'inventoryno': inventoryno});
            }
            return wikiArchivesAtLinks;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processwikiArchivesAtResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processMetagridResponse(metagridResult, ethConfigService, config){
        try{
            let sourcesWhitelist = config.whitelistMetagrid;
            let resources = metagridResult[0].resp.concordances[0].resources;
            let whitelistedMetagridLinks = [];
            let whitelistedMetagridLinksSorted = [];

            if (!!resources[0] && resources.length > 0) {
                let name = resources[0].last_name + ', ' + resources[0].first_name;

                for(var j = 0; j < resources.length; j++){
                    let resource = resources[j];
                    // https://api.metagrid.ch/providers.json
                    let slug = resource.provider.slug;
                    let url = resource.link.uri;
                    if (sourcesWhitelist.indexOf(slug) === -1) {
                        continue;
                    }
                    let label = slug;
                    try{
                        label = ethConfigService.getLabel(config.sourcesMetagrid, slug);
                    }
                    catch(e){}
                    whitelistedMetagridLinks.push({'slug': slug,'url': url, 'label': label});
                }
                // Dodis and HLS first
                let dodis = whitelistedMetagridLinks.filter(e => {
                    return e.slug === 'dodis';
                });
                whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(dodis);
                let hls = whitelistedMetagridLinks.filter(e => {
                    return e.slug === 'hls-dhs-dss';
                });
                whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(hls);
                let rest = whitelistedMetagridLinks.filter(e => {
                    return e.slug !== 'hls-dhs-dss' && e.slug !== 'dodis';
                });
                whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(rest);
            }
            return whitelistedMetagridLinksSorted;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processMetagridResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processPersonsResponse(results, ethConfigService, config, lang){
        try{
            if(!lang)lang = 'de';
            let person = {};
            person.gnd = "";
            let resultWithGnd = results.filter(e=>{
                return e.gnd && e.gnd != "";
            })
            if(resultWithGnd.length > 0){
                person.gnd = resultWithGnd[0].gnd;
            }
            // DNB Entityfacts
            let entityfactsResult = results.filter(e => {
                return e.provider === 'hub.culturegraph.org';
            });
            if(entityfactsResult.length > 0){
                person.entityfacts = this.processEntityfactsResponse(entityfactsResult);
            }

            // Metagrid
            let metagridResult = results.filter(e => {
                return e.provider === 'api.metagrid.ch';
            });
            if(metagridResult.length > 0 && metagridResult[0].resp.concordances && metagridResult[0].resp.concordances.length > 0){
                person.metagridLinks = this.processMetagridResponse(metagridResult, ethConfigService, config);
            }

            // Wikidata bio and Links
            let wikiResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("birth") > -1;
            });
            if(wikiResult.length > 0){
                person.wiki = this.processWikiResponse(wikiResult, ethConfigService, config);
            }

            // Wikidata archives at
            let wikiArchivesAtResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("refnode") > -1;
            });

            if(wikiArchivesAtResult.length > 0){
                person.wikiArchivesAtLinks = this.processwikiArchivesAtResponse(wikiArchivesAtResult);
            }
            return person;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processPersonsResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    /** @private */
    function keepResponse(response) {
        return response;
    }

    /** @private */
    function keepError(e) {
        console.error("ethPersonCardsService: rejected query: \n\n" + e.config.url);
        return e;
    }

    return {
        getPersonByGnd: getPersonByGnd,
        getGndByIdRef: getGndByIdRef,
        buildResult: buildResult,
        processWikiResponse: processWikiResponse,
        processwikiArchivesAtResponse: processwikiArchivesAtResponse,
        processEntityfactsResponse: processEntityfactsResponse,
        processMetagridResponse: processMetagridResponse,
        processPersonsResponse: processPersonsResponse
    };

}]
