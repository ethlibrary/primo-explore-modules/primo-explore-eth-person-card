# primo-explore-eth-person-card

## Description

This Module integrates a person card and a toggle link into the fullview in Primo VE.
It extracts the GND IDs (by default in local3) and IdRef IDs (local 90) and queries various APIs with it: Metagrid, Entityfacts (DNB), Wikidata Query Service.

### Screenshot

#### Person Card
![screenshot](https://gitlab.com/ethlibrary/primo-explore-modules/primo-explore-eth-person-card/-/raw/master/screenshot1.jpg)

## Installation

1. Assuming you've installed and are using [primo-explore-devenv](https://github.com/ExLibrisGroup/primo-explore-devenv).

2. Navigate to your view root directory. For example:
    ```
    cd primo-explore/custom/MY_VIEW_ID
    ```
3. If you do not already have a package.json file in this directory, create one:
    ```
    npm init -y
    ```
4. Install this package:
    ```
    npm install primo-explore-eth-person-card --save-dev
    ```

## Usage

Once installed, inject `ethPersonCardModule` as a dependency, and then add the eth-person-card-component directive to the prmServiceDetailsAfter component.

```js

import 'primo-explore-eth-person-card';

var app = angular.module('viewCustom', ['ethPersonCardModule'])
    .component('prmServiceDetailsAfter',  {
        bindings: {parentCtrl: '<'},
        template: `<eth-person-card-component after-ctrl="$ctrl"></eth-person-card-component>`
    })

```
## Configuration
1. Please copy the three images from the /img directory of node_modules/primo-explore-eth-person-card into the /img directory of your view.

2. With the whitelist for Metagrid the result is filtered.
The whitelists of providers whose links should be displayed can be changed in eth-person-card.config.js.
The array whitelistMetagrid contains the slugs of the respective Metagrid providers (https://api.metagrid.ch/providers.json). When you add a slug, you must also add the labels in the eth-person-card.config.js (sourcesMetagrid.label from providers.json: 'short_description').

3. The values of all labels are changeable in the eth-person-card.config.js .
